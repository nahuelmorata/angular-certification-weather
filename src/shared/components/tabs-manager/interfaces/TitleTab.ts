export interface TitleTab {
    title: string;
    hasRemove: boolean;
}
