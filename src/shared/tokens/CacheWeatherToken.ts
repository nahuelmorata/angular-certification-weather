import { InjectionToken } from "@angular/core";
import { CurrentConditions } from "app/current-conditions/current-conditions.type";
import { CacheService } from "shared/services/cache/cache.service";

export const CacheWeatherToken = new InjectionToken<CacheService<string, CurrentConditions>>('CacheWeatherToken');
