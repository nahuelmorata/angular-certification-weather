import { InjectionToken } from "@angular/core";
import { Forecast } from "app/forecasts-list/forecast.type";
import { CacheService } from "shared/services/cache/cache.service";

export const CacheForecastToken = new InjectionToken<CacheService<string, Forecast>>('CacheForecastToken');
