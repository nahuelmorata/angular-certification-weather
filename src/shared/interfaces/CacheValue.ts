export interface CacheValue<T> {
    createdAt: Date;
    value: T;
}
